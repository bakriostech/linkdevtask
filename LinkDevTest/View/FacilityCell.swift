//
//  FacilityCell.swift
//  LinkDevTest
//
//  Created by Bakr mohamed on 1/25/19.
//  Copyright © 2019 Bakr. All rights reserved.
//

import UIKit

class FacilityCell: UICollectionViewCell {
    
    @IBOutlet var facilityImage: UIImageView!
    @IBOutlet var trimmedDescription: UILabel!
    @IBOutlet var facilityTitle: UILabel!
    
}
