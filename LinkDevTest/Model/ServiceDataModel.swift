//
//  Data.swift
//
//  Created by Bakr mohamed on 1/24/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ServiceDataModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionEN = "DescriptionEN"
    static let videoID = "VideoID"
    static let imageSrc = "ImageSrc"
    static let eServiceRequestURL = "EServiceRequestURL"
    static let briefAR = "BriefAR"
    static let iD = "ID"
    static let titleTrimmedAR = "TitleTrimmedAR"
    static let titleTrimmed = "TitleTrimmed"
    static let parentDepartment = "ParentDepartment"
    static let hasSubServices = "hasSubServices"
    static let parentServiceID = "ParentServiceID"
    static let requiredDocuments = "RequiredDocuments"
    static let timeFrame = "TimeFrame"
    static let eServiceRequestURLAR = "EServiceRequestURLAR"
    static let titleEN = "TitleEN"
    static let detailsURL = "DetailsURL"
    static let policiesAndProceduresAR = "PoliciesAndProceduresAR"
    static let fees = "Fees"
    static let description = "Description"
    static let mobileImageSrc = "MobileImageSrc"
    static let eServiceRequestFullURL = "EServiceRequestFullURL"
    static let parentServiceAR = "ParentServiceAR"
    static let requiredDocumentsEN = "RequiredDocumentsEN"
    static let prerequisitesEN = "PrerequisitesEN"
    static let briefTrimmedEN = "BriefTrimmedEN"
    static let briefEN = "BriefEN"
    static let parentServiceEN = "ParentServiceEN"
    static let assistingDocumentURL = "AssistingDocumentURL"
    static let serviceChannelsEN = "ServiceChannelsEN"
    static let prerequisites = "Prerequisites"
    static let serviceChannelsAR = "ServiceChannelsAR"
    static let prerequisitesAR = "PrerequisitesAR"
    static let timeFrameAR = "TimeFrameAR"
    static let brief = "Brief"
    static let requiredDocumentsAR = "RequiredDocumentsAR"
    static let parentService = "ParentService"
    static let feesEN = "FeesEN"
    static let subServices = "subServices"
    static let briefTrimmed = "BriefTrimmed"
    static let timeFrameEN = "TimeFrameEN"
    static let policiesAndProceduresEN = "PoliciesAndProceduresEN"
    static let titleTrimmedEN = "TitleTrimmedEN"
    static let title = "Title"
    static let descriptionAR = "DescriptionAR"
    static let policiesAndProcedures = "PoliciesAndProcedures"
    static let eServiceRequestURLEN = "EServiceRequestURLEN"
    static let serviceChannels = "ServiceChannels"
    static let titleAR = "TitleAR"
    static let feesAR = "FeesAR"
  }

  // MARK: Properties
  public var descriptionEN: String?
  public var videoID: String?
  public var imageSrc: String?
  public var eServiceRequestURL: String?
  public var briefAR: String?
  public var iD: Int?
  public var titleTrimmedAR: String?
  public var titleTrimmed: String?
  public var parentDepartment: ParentDepartment?
  public var hasSubServices: Bool? = false
  public var parentServiceID: String?
  public var requiredDocuments: String?
  public var timeFrame: String?
  public var eServiceRequestURLAR: String?
  public var titleEN: String?
  public var detailsURL: String?
  public var policiesAndProceduresAR: String?
  public var fees: String?
  public var description: String?
  public var mobileImageSrc: String?
  public var eServiceRequestFullURL: String?
  public var parentServiceAR: String?
  public var requiredDocumentsEN: String?
  public var prerequisitesEN: String?
  public var briefTrimmedEN: String?
  public var briefEN: String?
  public var parentServiceEN: String?
  public var assistingDocumentURL: String?
  public var serviceChannelsEN: String?
  public var prerequisites: String?
  public var serviceChannelsAR: String?
  public var prerequisitesAR: String?
  public var timeFrameAR: String?
  public var brief: String?
  public var requiredDocumentsAR: String?
  public var parentService: String?
  public var feesEN: String?
  public var subServices: [Any]?
  public var briefTrimmed: String?
  public var timeFrameEN: String?
  public var policiesAndProceduresEN: String?
  public var titleTrimmedEN: String?
  public var title: String?
  public var descriptionAR: String?
  public var policiesAndProcedures: String?
  public var eServiceRequestURLEN: String?
  public var serviceChannels: String?
  public var titleAR: String?
  public var feesAR: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    descriptionEN = json[SerializationKeys.descriptionEN].string
    videoID = json[SerializationKeys.videoID].string
    imageSrc = json[SerializationKeys.imageSrc].string
    eServiceRequestURL = json[SerializationKeys.eServiceRequestURL].string
    briefAR = json[SerializationKeys.briefAR].string
    iD = json[SerializationKeys.iD].int
    titleTrimmedAR = json[SerializationKeys.titleTrimmedAR].string
    titleTrimmed = json[SerializationKeys.titleTrimmed].string
    parentDepartment = ParentDepartment(json: json[SerializationKeys.parentDepartment])
    hasSubServices = json[SerializationKeys.hasSubServices].boolValue
    parentServiceID = json[SerializationKeys.parentServiceID].string
    requiredDocuments = json[SerializationKeys.requiredDocuments].string
    timeFrame = json[SerializationKeys.timeFrame].string
    eServiceRequestURLAR = json[SerializationKeys.eServiceRequestURLAR].string
    titleEN = json[SerializationKeys.titleEN].string
    detailsURL = json[SerializationKeys.detailsURL].string
    policiesAndProceduresAR = json[SerializationKeys.policiesAndProceduresAR].string
    fees = json[SerializationKeys.fees].string
    description = json[SerializationKeys.description].string
    mobileImageSrc = json[SerializationKeys.mobileImageSrc].string
    eServiceRequestFullURL = json[SerializationKeys.eServiceRequestFullURL].string
    parentServiceAR = json[SerializationKeys.parentServiceAR].string
    requiredDocumentsEN = json[SerializationKeys.requiredDocumentsEN].string
    prerequisitesEN = json[SerializationKeys.prerequisitesEN].string
    briefTrimmedEN = json[SerializationKeys.briefTrimmedEN].string
    briefEN = json[SerializationKeys.briefEN].string
    parentServiceEN = json[SerializationKeys.parentServiceEN].string
    assistingDocumentURL = json[SerializationKeys.assistingDocumentURL].string
    serviceChannelsEN = json[SerializationKeys.serviceChannelsEN].string
    prerequisites = json[SerializationKeys.prerequisites].string
    serviceChannelsAR = json[SerializationKeys.serviceChannelsAR].string
    prerequisitesAR = json[SerializationKeys.prerequisitesAR].string
    timeFrameAR = json[SerializationKeys.timeFrameAR].string
    brief = json[SerializationKeys.brief].string
    requiredDocumentsAR = json[SerializationKeys.requiredDocumentsAR].string
    parentService = json[SerializationKeys.parentService].string
    feesEN = json[SerializationKeys.feesEN].string
    if let items = json[SerializationKeys.subServices].array { subServices = items.map { $0.object} }
    briefTrimmed = json[SerializationKeys.briefTrimmed].string
    timeFrameEN = json[SerializationKeys.timeFrameEN].string
    policiesAndProceduresEN = json[SerializationKeys.policiesAndProceduresEN].string
    titleTrimmedEN = json[SerializationKeys.titleTrimmedEN].string
    title = json[SerializationKeys.title].string
    descriptionAR = json[SerializationKeys.descriptionAR].string
    policiesAndProcedures = json[SerializationKeys.policiesAndProcedures].string
    eServiceRequestURLEN = json[SerializationKeys.eServiceRequestURLEN].string
    serviceChannels = json[SerializationKeys.serviceChannels].string
    titleAR = json[SerializationKeys.titleAR].string
    feesAR = json[SerializationKeys.feesAR].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionEN { dictionary[SerializationKeys.descriptionEN] = value }
    if let value = videoID { dictionary[SerializationKeys.videoID] = value }
    if let value = imageSrc { dictionary[SerializationKeys.imageSrc] = value }
    if let value = eServiceRequestURL { dictionary[SerializationKeys.eServiceRequestURL] = value }
    if let value = briefAR { dictionary[SerializationKeys.briefAR] = value }
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = titleTrimmedAR { dictionary[SerializationKeys.titleTrimmedAR] = value }
    if let value = titleTrimmed { dictionary[SerializationKeys.titleTrimmed] = value }
    if let value = parentDepartment { dictionary[SerializationKeys.parentDepartment] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.hasSubServices] = hasSubServices
    if let value = parentServiceID { dictionary[SerializationKeys.parentServiceID] = value }
    if let value = requiredDocuments { dictionary[SerializationKeys.requiredDocuments] = value }
    if let value = timeFrame { dictionary[SerializationKeys.timeFrame] = value }
    if let value = eServiceRequestURLAR { dictionary[SerializationKeys.eServiceRequestURLAR] = value }
    if let value = titleEN { dictionary[SerializationKeys.titleEN] = value }
    if let value = detailsURL { dictionary[SerializationKeys.detailsURL] = value }
    if let value = policiesAndProceduresAR { dictionary[SerializationKeys.policiesAndProceduresAR] = value }
    if let value = fees { dictionary[SerializationKeys.fees] = value }
    if let value = description { dictionary[SerializationKeys.description] = value }
    if let value = mobileImageSrc { dictionary[SerializationKeys.mobileImageSrc] = value }
    if let value = eServiceRequestFullURL { dictionary[SerializationKeys.eServiceRequestFullURL] = value }
    if let value = parentServiceAR { dictionary[SerializationKeys.parentServiceAR] = value }
    if let value = requiredDocumentsEN { dictionary[SerializationKeys.requiredDocumentsEN] = value }
    if let value = prerequisitesEN { dictionary[SerializationKeys.prerequisitesEN] = value }
    if let value = briefTrimmedEN { dictionary[SerializationKeys.briefTrimmedEN] = value }
    if let value = briefEN { dictionary[SerializationKeys.briefEN] = value }
    if let value = parentServiceEN { dictionary[SerializationKeys.parentServiceEN] = value }
    if let value = assistingDocumentURL { dictionary[SerializationKeys.assistingDocumentURL] = value }
    if let value = serviceChannelsEN { dictionary[SerializationKeys.serviceChannelsEN] = value }
    if let value = prerequisites { dictionary[SerializationKeys.prerequisites] = value }
    if let value = serviceChannelsAR { dictionary[SerializationKeys.serviceChannelsAR] = value }
    if let value = prerequisitesAR { dictionary[SerializationKeys.prerequisitesAR] = value }
    if let value = timeFrameAR { dictionary[SerializationKeys.timeFrameAR] = value }
    if let value = brief { dictionary[SerializationKeys.brief] = value }
    if let value = requiredDocumentsAR { dictionary[SerializationKeys.requiredDocumentsAR] = value }
    if let value = parentService { dictionary[SerializationKeys.parentService] = value }
    if let value = feesEN { dictionary[SerializationKeys.feesEN] = value }
    if let value = subServices { dictionary[SerializationKeys.subServices] = value }
    if let value = briefTrimmed { dictionary[SerializationKeys.briefTrimmed] = value }
    if let value = timeFrameEN { dictionary[SerializationKeys.timeFrameEN] = value }
    if let value = policiesAndProceduresEN { dictionary[SerializationKeys.policiesAndProceduresEN] = value }
    if let value = titleTrimmedEN { dictionary[SerializationKeys.titleTrimmedEN] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = descriptionAR { dictionary[SerializationKeys.descriptionAR] = value }
    if let value = policiesAndProcedures { dictionary[SerializationKeys.policiesAndProcedures] = value }
    if let value = eServiceRequestURLEN { dictionary[SerializationKeys.eServiceRequestURLEN] = value }
    if let value = serviceChannels { dictionary[SerializationKeys.serviceChannels] = value }
    if let value = titleAR { dictionary[SerializationKeys.titleAR] = value }
    if let value = feesAR { dictionary[SerializationKeys.feesAR] = value }
    return dictionary
  }

}
