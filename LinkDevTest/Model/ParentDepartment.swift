//
//  ParentDepartment.swift
//
//  Created by Bakr mohamed on 1/24/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ParentDepartment {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let briefTrimmed = "BriefTrimmed"
    static let iD = "ID"
    static let titleAR = "TitleAR"
    static let titleEN = "TitleEN"
    static let title = "Title"
    static let imageSrc = "ImageSrc"
    static let brief = "Brief"
    static let briefEN = "BriefEN"
    static let briefAR = "BriefAR"
    static let briefTrimmedEN = "BriefTrimmedEN"
  }

  // MARK: Properties
  public var briefTrimmed: String?
  public var iD: Int?
  public var titleAR: String?
  public var titleEN: String?
  public var title: String?
  public var imageSrc: String?
  public var brief: String?
  public var briefEN: String?
  public var briefAR: String?
  public var briefTrimmedEN: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    briefTrimmed = json[SerializationKeys.briefTrimmed].string
    iD = json[SerializationKeys.iD].int
    titleAR = json[SerializationKeys.titleAR].string
    titleEN = json[SerializationKeys.titleEN].string
    title = json[SerializationKeys.title].string
    imageSrc = json[SerializationKeys.imageSrc].string
    brief = json[SerializationKeys.brief].string
    briefEN = json[SerializationKeys.briefEN].string
    briefAR = json[SerializationKeys.briefAR].string
    briefTrimmedEN = json[SerializationKeys.briefTrimmedEN].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = briefTrimmed { dictionary[SerializationKeys.briefTrimmed] = value }
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = titleAR { dictionary[SerializationKeys.titleAR] = value }
    if let value = titleEN { dictionary[SerializationKeys.titleEN] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = imageSrc { dictionary[SerializationKeys.imageSrc] = value }
    if let value = brief { dictionary[SerializationKeys.brief] = value }
    if let value = briefEN { dictionary[SerializationKeys.briefEN] = value }
    if let value = briefAR { dictionary[SerializationKeys.briefAR] = value }
    if let value = briefTrimmedEN { dictionary[SerializationKeys.briefTrimmedEN] = value }
    return dictionary
  }

}
