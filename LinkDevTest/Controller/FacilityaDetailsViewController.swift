//
//  DetailsViewController.swift
//  LinkDevTest
//
//  Created by Bakr mohamed on 1/25/19.
//  Copyright © 2019 Bakr. All rights reserved.
//

import UIKit

class FacilityaDetailsViewController : UIViewController {
    
    //IBOUltls
    @IBOutlet var facilityImage: UIImageView!
    @IBOutlet var facilityDecription: UILabel!
    @IBOutlet var facilityPrequisits: UILabel!
    @IBOutlet var facilityRequiredDocs: UILabel!
    @IBOutlet var facilityFees: UILabel!
    @IBOutlet var facilityTimeFrams: UILabel!
    @IBOutlet var facilityServiceChannel: UILabel!
    @IBOutlet var facilityPolicies: UILabel!
    @IBOutlet var facilityAssistDocs: UILabel!
    
    //variable
    var imageData : UIImage?
    var fdescription : String?
    var prequisit : String?
    var requiredDocs : String?
    var fees : String?
    var time : String?
    var service : String?
    var policies : String?
    var assisDocs : String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // remove back button title and icon
        let backImage = UIImage(named: "back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        //end edit navigation Bar
        
        //update UI
        updateUI()
        
    }
    
    func updateUI(){
        facilityImage.image = imageData
        facilityDecription.setHTMLFromString(text: fdescription!.replacingOccurrences(of: "For more details click here", with: "")) 
        facilityPrequisits.setHTMLFromString(text: prequisit!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityRequiredDocs.setHTMLFromString(text: requiredDocs!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityFees.setHTMLFromString(text: fees!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityTimeFrams.setHTMLFromString(text:  time!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityServiceChannel.setHTMLFromString(text: service!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityPolicies.setHTMLFromString(text: policies!.replacingOccurrences(of: "For more details click here", with: ""))
        facilityAssistDocs.setHTMLFromString(text: assisDocs!.replacingOccurrences(of: "For more details click here", with: ""))
    }
    
    
}
