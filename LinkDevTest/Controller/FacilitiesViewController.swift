//
//  ViewController.swift
//  LinkDevTest
//
//  Created by Bakr mohamed on 1/25/19.
//  Copyright © 2019 Bakr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FacilitiesViewController: UIViewController {
    
    // Variables
    let serviceURL = "https://dhcr.gov.ae/MobileWebAPI/api/Common/ServiceCatalogue/GetDepartmentServices"
    let postParams:NSDictionary = [
        "DepartmentID": 2,
        "PageSize" : 10,
        "PageIndex" : 1
    ]
    var recivedData : [ServiceDataModel] = []
    var facilityDetailedData : ServiceDataModel?
    
    // Outlets
    @IBOutlet var facilitiesCollectionView: UICollectionView!
    @IBOutlet var activityIndecator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        facilitiesCollectionView.isHidden = true
        activityIndecator.startAnimating()
        
        Alamofire.request(serviceURL, method: .post, parameters: postParams as? [String : AnyObject], encoding: JSONEncoding.default, headers: ["Content-Type":"text/html; charset=us-ascii"])
            .responseString { respone in switch respone.result{
                
            case .success(let stringJson):
                let json = stringJson.data(using: String.Encoding.utf8).flatMap({try? JSON(data: $0)}) ?? JSON(NSNull())
                self.recivedData = ServiceClass(json: json).data!
            case.failure(let error):
                print(error)
                }
                print(self.recivedData.count)
                self.facilitiesCollectionView.reloadData()
                self.activityIndecator.stopAnimating()
                self.activityIndecator.isHidden = true
                self.facilitiesCollectionView.isHidden = false
        }
        
        
        facilitiesCollectionView.dataSource = self
        facilitiesCollectionView.delegate = self
        
    }
    
    
}

extension FacilitiesViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return recivedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "facilityCell", for: indexPath) as! FacilityCell
        
        //set cell Image from API reciveddata
        let imageURL = URL(string: recivedData[indexPath.row].mobileImageSrc!)
        if let data = try? Data(contentsOf: imageURL!)
        {
            let imageData: UIImage = UIImage(data: data)!
            cell.facilityImage.image = imageData
        }
        
        // set cell title
        cell.facilityTitle.text = recivedData[indexPath.row].titleEN
        
        // set cell facility description
        cell.trimmedDescription.text = recivedData[indexPath.row].briefTrimmedEN
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            
            let finalCellFrame: CGRect? = cell.frame
            //check the scrolling direction to verify from which side of the screen the cell should come.
            let translation: CGPoint = collectionView.panGestureRecognizer.translation(in: collectionView.superview)
            if translation.x > 0 {
                cell.frame = CGRect(x: (finalCellFrame?.origin.x ?? 0.0) - 1000, y: -500.0, width: 0, height: 0)
            } else {
                cell.frame = CGRect(x: (finalCellFrame?.origin.x ?? 0.0) + 1000, y: -500.0, width: 0, height: 0)
            }
            
            UIView.animate(withDuration: 0.8, animations: {
                cell.frame = finalCellFrame ?? CGRect.zero
            })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        facilityDetailedData = recivedData[indexPath.row]
        performSegue(withIdentifier: "FacilityDetailedVC", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FacilityDetailedVC" {
            let facilityDetailsVC = segue.destination as! FacilityaDetailsViewController
            if facilityDetailedData != nil {
                let imageURL = URL(string: facilityDetailedData!.mobileImageSrc!)
                if let data = try? Data(contentsOf: imageURL!)
                {
                    facilityDetailsVC.imageData = UIImage(data: data)!
                }
                facilityDetailsVC.fdescription = facilityDetailedData!.description!
                facilityDetailsVC.prequisit = facilityDetailedData!.prerequisites!
                facilityDetailsVC.requiredDocs = facilityDetailedData!.requiredDocuments!
                facilityDetailsVC.fees = facilityDetailedData!.fees!.html2String
                facilityDetailsVC.time = facilityDetailedData!.timeFrame!
                facilityDetailsVC.service = facilityDetailedData!.serviceChannels!
                facilityDetailsVC.policies = facilityDetailedData!.policiesAndProcedures!
                 if facilityDetailedData!.assistingDocumentURL == nil {
                    facilityDetailsVC.assisDocs = "-"
                }
                facilityDetailsVC.title = facilityDetailedData?.title
            }
            
        }
        
    }
    
    
}
